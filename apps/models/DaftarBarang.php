<?php
    class DaftarBarang{
        private $data_barang;
        public function __construct(){
            $this->data_barang = [
                [
                    'id' => 101,
                    'nama' => 'beras',
                    'kuantitas' => 100
                ],
                [
                    'id' => 102,
                    'nama' => 'gula',
                    'kuantitas' => 100
                ],
                [
                    'id' => 103,
                    'nama' => 'kopi',
                    'kuantitas' => 100
                ]
                ];
        }
        public function GetDaftarBarang(){
            return $this->data_barang;
        }
    }
?>