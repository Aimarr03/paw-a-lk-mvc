<?php
    require 'apps/Controller.php';
    class Index extends Controller{
        private $data;
        private $databarang;
        public function __construct()
        {
            $this->data = $this->CheckModel("Barang");
            $this->databarang = $this->CheckModel("DaftarBarang");
        }
        public function index(){
            echo "anda sedang berada di Home action\n";
        }

        public function home($param1, $param2){
            echo "anda sedang berada di Home action dengan param {$param1}, {$param2}";
        }
        public function displaydata(){
            $data =  $this->data->GetData();
            $this->loadview('templates/header',['title'=>'list barang']);
            $this->loadview('home/detailbarang',$data);
            $this->loadview('templates/footer');
            echo "<br>";
            $this->displayDaftarData();
        }
        public function displayDaftarData(){
            $data = $this->databarang->GetDaftarBarang();
            $this->loadview('templates/header',['title'=>'list barang']);
            $this->loadview('home/listbarang',$data);
            $this->loadview('templates/footer');
        }
    }
?>