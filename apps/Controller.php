<?php
    class Controller{
        public function CheckModel($args){
            if(file_exists('apps/models/'.$args.'.php')){
                require_once('apps/models/'.$args.'.php');
                $args = new $args;
            }
            return $args;
        }
        public function loadview($args, $data=null){
            if(file_exists('apps/views/'.$args.'.php')){
                require_once('apps/views/'.$args.'.php');
            }
        }
    }
?>